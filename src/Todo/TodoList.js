import React, { Component } from "react";

export default class TodoList extends Component {
  state = {
    userInput: "",
    items: [],
  };

  onChange(event) {
    this.setState(
      {
        userInput: event.target.value,
      }
      //   () => console.log(this.state.userInput)
    );
  }

  addTodo(event) {
    //event.preventDefault() => ne pas reload la page
    event.preventDefault();
    this.setState({
      userInput: "",
      items: [...this.state.items, this.state.userInput],
    });
  }

  deleteTodo(item) {
    const array = this.state.items;
    const index = array.indexOf(item);
    //supprime
    array.splice(index, 1);
    this.setState({
      items: array,
    });
  }
  renderTodos() {
    return this.state.items.map((item) => {
      return (
        <div key={item}>
          {item} | <button onClick={this.deleteTodo.bind(this, item)}>X</button>
        </div>
      );
    });
  }
  render() {
    return (
      <div>
        <h1>TodoList</h1>
        <form>
          <p>Item a ajouter :</p>
          <input
            value={this.state.userInput}
            type="text"
            placeholder="mettre un item"
            onChange={this.onChange.bind(this)}
          />
          <button className="ajout" onClick={this.addTodo.bind(this)}>
            Ajouter
          </button>
        </form>
        <div className="itemslist">{this.renderTodos()}</div>
      </div>
    );
  }
}
